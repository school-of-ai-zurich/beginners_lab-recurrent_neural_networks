## School of AI Z&uuml;rich

**Beginner's Lab - Recurrent Neural Networks**

Using Python and Tensorflow 2.0

---

### Install (notes for installation steps in [`install_notes.md`](install_notes.md)
Linux/MacOS

> bash install.sh

### Run
Linux/MacOS

> bash start.sh

---
![soai](images/soai_logo.png)
